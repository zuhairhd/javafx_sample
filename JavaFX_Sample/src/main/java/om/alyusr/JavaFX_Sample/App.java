package om.alyusr.JavaFX_Sample;

/**
 * Hello world!
 *
 */
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import javafx.scene.layout.StackPane;


@SuppressWarnings("restriction")
public class App extends Application {
	@Override
	public void start(Stage primarystage) {
		
		StackPane sp = new StackPane();
		sp.getChildren();
		
		Button mybtn = new Button("OK");
		Scene myscene = new Scene(mybtn,300,350);
		primarystage.setTitle("My JavaFX");
		primarystage.setScene(myscene);
		primarystage.show();
		
		Stage stg = new Stage();
		stg.setTitle("I am Second Window");
		stg.setScene(new Scene(new Button("Zuhair"),200,250));
		stg.show();

	}

	public static void main(String[] args) {
		// System.out.println( "Hello World!" );
		Application.launch(args);
	}
}
